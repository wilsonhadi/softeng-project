package Tasks;

import java.text.SimpleDateFormat;
import java.util. *;

public class Task {
    private String title;
    private TaskSubject subject;
    private Date deadline;
    private SimpleDateFormat dateToStr = new SimpleDateFormat("dd/MM/yyyy");

    public Task(String title, TaskSubject subject, Date deadline) {
        this.title = title;
        this.subject = subject;
        this.deadline = deadline;
    }

    public String getTitle() { return this.title; }

    public TaskSubject getSubject() {return this.subject; }

    public String getDeadlineStr() {
        return dateToStr.format(this.deadline);
    }
}