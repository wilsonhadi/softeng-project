package Tasks;

import java.util. *;

public class TaskSubject {
    private String name;
    private ArrayList<Task> tasks;

    public TaskSubject(String name) {
        this.name = name;
        this.tasks = new ArrayList<>();
    }

    public void addTask(Task task) { tasks.add(task); }

    public ArrayList<Task> getTasks() {return this.tasks; }

    public String getName() { return this.name; }
}