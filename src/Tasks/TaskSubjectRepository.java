package Tasks;
import Tasks.Task;
import Tasks.TaskSubject;
import Printer.Printer;

import java.util.ArrayList;
import java.util.HashMap;

public class TaskSubjectRepository {
    private ArrayList<Task> tasks;
    private HashMap<String, TaskSubject> subjects;
    private Printer p = new Printer();

    public TaskSubjectRepository() {
        this.tasks = new ArrayList<>();
        this.subjects = new HashMap<>();
    }

    public ArrayList<Task> getTasks() {
        return tasks;
    }

    public HashMap<String, TaskSubject> getSubjects() {
        return subjects;
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    public void addSubject(TaskSubject subject) {
        subjects.put(subject.getName(), subject);
    }
}
