package Printer;
import Tasks.*;

import java.util.ArrayList;
import java.util.Set;

public class Printer {
    public Printer() {}

    public void print(String text) { System.out.print(text); }

    public void println(String text) { System.out.println(text); }

    public void printBlank() { System.out.println(); }

    public void printLines() { println(String.format("%-55s", "").replace(" ", "-")); }

    public void subjectNotExist() {
        printBlank();
        println("Subject does not exist! Please enter the *full* and *exact* subject name");
        println("Enter 'subjects' command to view the existing subjects");
        printBlank();
    }

    public void printTask(ArrayList<Task> tasks) {
        printBlank();
        println("Title | Subject | Deadline");
        println(String.format("%-36s", "").replace(" ", "-"));
        String taskCapitalised;
        for (Task task : tasks) {
            taskCapitalised = task.getTitle().substring(0,1).toUpperCase() + task.getTitle().substring(1).toLowerCase();
            println(taskCapitalised + " | " + capitalise(task.getSubject().getName()) + " | " + task.getDeadlineStr());
        }
        printBlank();
    }

    public void printSubject(Set<String> subjects) {
        println("Available Subjects");
        println(String.format("%-36s", "").replace(" ", "-"));
        for (String subject: subjects) {
            println(capitalise(subject));
        }
        printBlank();
    }

    public String capitalise(String input) {
        String result = "";
        String capitalised;
        String[] inputArr = input.split(" ");
        for (String word: inputArr) {
            capitalised = word.substring(0,1).toUpperCase() + word.substring(1).toLowerCase();
            result += capitalised + " ";
        }
        return result;
    }
}
