package Test;
import Tasks.Task;
import Tasks.TaskSubject;
import Tasks.TaskSubjectRepository;
import Printer.Printer;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AddTaskTest {
    private static Printer p = new Printer();

    private static TaskSubjectRepository taskSubjectRepository = new TaskSubjectRepository();
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public static void main(String[] args) throws Exception {
        try {
            p.printBlank();
            p.println("Testing adding tasks.....");

            // making new task and subject
            TaskSubject subject1 = new TaskSubject("subject1");
            taskSubjectRepository.addSubject(subject1);
            Date deadline1 = dateFormat.parse("01/02/2020");
            Task task1 = new Task("task1", subject1, deadline1);
            taskSubjectRepository.addTask(task1);
            if (taskSubjectRepository.getTasks().contains(task1)) {
                p.println("...task1 added!...");
            }

            // make new task with same subject as task1
            Date deadline2 = dateFormat.parse("01/03/2020");
            Task task2 = new Task("task2", subject1, deadline2);
            taskSubjectRepository.addTask(task2);
            if (taskSubjectRepository.getTasks().contains(task2)){
                p.println("...task2 added!...");
            }

            // make new task with different subject as task1
            TaskSubject subject2 = new TaskSubject("subject2");
            taskSubjectRepository.addSubject(subject2);
            Date deadline3 = dateFormat.parse("01/07/2020");
            Task task3 = new Task("task3", subject2, deadline3);
            taskSubjectRepository.addTask(task3);
            if (taskSubjectRepository.getTasks().contains(task3)){
                p.println("...task3 added!...");
            }

            // test to see if tasks and subjects were successfully made and stored
            if (taskSubjectRepository.getTasks().size() == 3) {
                p.println(".....Tasks added successfully");
            } else { p.println(".....Adding tasks failed"); }
            p.printBlank();
            p.println("Testing adding subjects.....");
            if (taskSubjectRepository.getSubjects().size() == 2) {
                if (taskSubjectRepository.getSubjects().containsValue(subject1)) {
                    p.println("...subject1 added!...");
                }
                if (taskSubjectRepository.getSubjects().containsValue(subject2)) {
                    p.println("...subject2 added!...");
                }
                p.println(".....Subjects added successfully");
            } else { p.println(".....Adding subjects failed"); }
            p.printBlank();
            p.println("------------Testing completed------------");

        } catch (Exception e) {
            p.println("Something went wrong.....");
            p.printBlank();
        }
    }
}
