package Test;
import Tasks.Task;
import Tasks.TaskSubject;
import Tasks.TaskSubjectRepository;
import Printer.Printer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FilterTaskTest {
    private static Printer p = new Printer();

    private static TaskSubjectRepository taskSubjectRepository = new TaskSubjectRepository();
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public static void main(String[] args) throws Exception {
        try {
            // making new task and subject
            TaskSubject subject1 = new TaskSubject("subject1");
            Date deadline1 = dateFormat.parse("01/02/2020");
            Task task1 = new Task("task1", subject1, deadline1);
            subject1.addTask(task1);
            taskSubjectRepository.addSubject(subject1);
            taskSubjectRepository.addTask(task1);

            // make new task with same subject as task1
            Date deadline2 = dateFormat.parse("01/03/2020");
            Task task2 = new Task("task2", subject1, deadline2);
            taskSubjectRepository.getSubjects().get("subject1").addTask(task2);
            taskSubjectRepository.addTask(task2);

            // make new task with different subject as task1
            TaskSubject subject2 = new TaskSubject("subject2");
            Date deadline3 = dateFormat.parse("01/07/2020");
            Task task3 = new Task("task3", subject2, deadline3);
            subject2.addTask(task3);
            taskSubjectRepository.addSubject(subject2);
            taskSubjectRepository.addTask(task3);

            // test to see if tasks can be filtered by subject
            ArrayList<Task> tasksOfSubject1 = taskSubjectRepository.getSubjects().get("subject1").getTasks();
            ArrayList<Task> tasksOfSubject2 = taskSubjectRepository.getSubjects().get("subject2").getTasks();
            Boolean pass = false;
            p.printBlank();
            p.println("Testing filtering tasks.....");
            if (tasksOfSubject1.size() == 2 && tasksOfSubject1.contains(task1) && tasksOfSubject1.contains(task2)) {
                p.println("...tasks of subject1 successfully filtered...");
                if (tasksOfSubject2.size() == 1 && tasksOfSubject2.contains(task3)) {
                    p.println("...tasks of subject2 successfully filtered...");
                    pass = true;
                }
            }
            if (pass) {
                p.println(".....all tasks successfully filtered");
            } else {
                p.println(".....tasks were not filtered correctly");
            }
            p.printBlank();
            p.println("------------Testing completed------------");

        } catch (Exception e) {
            p.println("Something went wrong.....");
            p.printBlank();
        }
    }
}
