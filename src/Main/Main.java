package Main;
import Tasks.Task;
import Tasks.TaskSubject;
import Printer.Printer;
import Tasks.TaskSubjectRepository;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util. *;

public class Main {
    private static InputReader in;
    private static PrintWriter out;
    // using printer class from Printer module
    private static Printer p = new Printer();

    // have a repository to store the tasks and subjects
    private static TaskSubjectRepository taskSubjectRepository = new TaskSubjectRepository();

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public static void main(String[] args) throws Exception {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        readCommand();
        out.close();
    }

    private static void readCommand() throws Exception{
        boolean running = true;
        while (running) {
            // reading the commands entered
            p.println("Type 'help' for list of commands");
            p.print("Type in your command: " );
            String input = in.nextLine();
            if (isBlank(input) || isInvalid(input)) { continue; }
            p.printBlank();

            Task task;
            TaskSubject subject;
            Date deadline;

            // executing appropriate commands
            switch (input) {
                case "help":
                    p.println("Commands:");
                    p.println(" - Add a task (addTask)");
                    p.println(" - Add a subject (addSubject)");
                    p.println(" - Show list of tasks (show)");
                    p.println(" - Show list of subject (subjects)");
                    p.println(" - Exit (exit)");
                    p.printBlank();

                    break;

                case "addTask":
                    p.println("--------------------Adding New Task--------------------");
                    // have user input task details
                    p.print("Task title: ");
                    String title = in.nextLine().toLowerCase();
                    if (isBlank(title)) { break; }

                    p.print("Subject name: ");
                    String subjectTitle = in.nextLine().toLowerCase();
                    if (isBlank(subjectTitle)) { break; }

                    p.print("Deadline (dd/mm/yyyy): ");
                    String deadlineStr = in.nextLine();
                    if (isBlank(deadlineStr)) { break; }

                    p.printBlank();
                    p.println("Making new task.....");

                    // setting the date of deadline
                    // checking if input has valid format
                    if (validateDate(deadlineStr) == null) {
                        break;
                    } else {
                        deadline = validateDate(deadlineStr);
                    }

                    // make and store Task and Subject
                    subject = assignSubject(subjectTitle);

                    // store the new task
                    task = new Task(title, subject, deadline);
                    taskSubjectRepository.addTask(task);
                    subject.addTask(task);
                    p.println("Task saved!");
                    p.printBlank();

                    break;

                case "addSubject":
                    p.println("--------------------Adding New Subject--------------------");
                    // have user subject name
                    p.print("Subject Name: ");
                    String name = in.nextLine().toLowerCase();
                    if (isBlank(name)) { break; }

                    // if subject doesn't already exist, make new one
                    if (isSubjectNotExist(name)) {
                        TaskSubject newSubject = new TaskSubject(name);
                        taskSubjectRepository.addSubject(newSubject);
                        p.printBlank();
                        p.println("Subject saved!");
                    } else {
                        p.printBlank();
                        p.println("A subject with that name already exists");
                        p.println("Enter 'subjects' command to view the existing subjects");
                        p.printBlank();
                    }

                    break;

                case "show":
                    // check first if there are any tasks to show
                    if (taskSubjectRepository.getTasks().size() == 0) {
                        p.println("There are no tasks to show!");
                        p.printBlank();
                        break;
                    }

                    ArrayList<Task> tasksToBePrinted;

                    // ask user if they would like to filter the tasks based on subject
                    p.print("Filter by subject? (y/n?): ");
                    input = in.nextLine().toLowerCase();
                    if (isBlank(input) && isInvalid(input)) { break; }
                    // check if user enters y or n correctly
                    if (!input.equals("y") && !input.equals("n")) {
                        p.printBlank();
                        p.println("Please type 'y' for yes, 'n' for no");
                        p.printBlank();
                        break;
                    }

                    if (input.equals("y")) {
                        p.print("Please enter (full and exact) subject name: ");
                        input = in.nextLine().toLowerCase();
                        if (isBlank(input)) { break; }
                        if (isSubjectNotExist(input)) {
                            p.subjectNotExist();
                            break;
                        }
                        // if subject exists, get the tasks
                        tasksToBePrinted = taskSubjectRepository.getSubjects().get(input).getTasks();
                    } else { tasksToBePrinted = taskSubjectRepository.getTasks(); }

                    p.printTask(tasksToBePrinted);

                    break;

                case "subjects":
                    // check if there are any subjects to show
                    if (taskSubjectRepository.getSubjects().size() == 0) {
                        p.printBlank();
                        p.println("There are no subjects to show!");
                        p.printBlank();
                    }

                    Set<String> subjectSet = taskSubjectRepository.getSubjects().keySet();
                    p.printSubject(subjectSet);

                    break;

                case "exit":
                    running = false;
                    break;

                default:
                    p.println("Please type a valid command.....");
                    p.printBlank();
            }

            p.printLines();

        }
        p.println("Thank you for using Task Viewer!");
        p.printBlank();
        out.close();
    }

    public static Boolean isBlank(String command) {
        if (command.equals("")) {
            p.printBlank();
            p.println("Please do not leave any fields blank.....");
            p.printBlank();
            return true;
        }
        return false;
    }

    public static Boolean isInvalid(String input) {
        if (input.contains(" ")) {
            p.printBlank();
            p.println("Please input only one command/input (no space (' ') as well).....");
            p.printBlank();
            return true;
        }
        return false;
    }

    public static Date validateDate(String strDate) throws Exception{
        try {
            Date deadline;
            String[] dateArr = strDate.split("/");
            int dd = Integer.parseInt(dateArr[0]);
            int mm = Integer.parseInt(dateArr[1]);
            int yyyy = Integer.parseInt(dateArr[2]);
            if (dd > 31 || mm > 12 || yyyy < 2019 || yyyy > 9999 || dd > 28 && mm == 2) {
                deadline = dateFormat.parse("Error");
            } else {
                deadline = dateFormat.parse(strDate);
            }
            return deadline;
        } catch (ParseException e) {
            p.printBlank();
            p.println("Error: Incorrect date format");
            p.printBlank();
            return null;
        }
    }

    public static Boolean isSubjectNotExist(String subject) {
        if (!taskSubjectRepository.getSubjects().containsKey(subject)) {
            return true;
        }
        return false;
    }

    public static TaskSubject assignSubject(String subjectTitle) {
        TaskSubject subject;
        if (isSubjectNotExist(subjectTitle)) {
            // if subject is new
            subject = new TaskSubject(subjectTitle);
            taskSubjectRepository.addSubject(subject);
        } else {
            subject = taskSubjectRepository.getSubjects().get(subjectTitle);
        }
        return subject;
    }

    // taken from https://codeforces.com/submissions/Petr
    // only to speed up process of reading user input
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
        public String nextLine() throws IOException{
            return reader.readLine();
        }
    }
}
